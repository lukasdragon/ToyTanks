<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.0" name="Ground" tilewidth="32" tileheight="32" tilecount="3" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="3">
  <image width="32" height="32" source="../../Unity/Assets/Textures/Map/Ground/grass.png"/>
 </tile>
 <tile id="5">
  <image width="32" height="32" source="../../Unity/Assets/Textures/Map/Ground/Stone.png"/>
 </tile>
 <tile id="4">
  <image width="32" height="32" source="../../Unity/Assets/Textures/Map/Ground/Hole.png"/>
 </tile>
</tileset>
