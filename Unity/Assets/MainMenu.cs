using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ToyTanks
{
    public class MainMenu : MonoBehaviour
    {
        public void JoinGame()
        {
            NetworkManager.singleton.StartClient();
        }

        public void SetAddress(string address)
        {
            NetworkManager.singleton.networkAddress = address;
        }

        public void LoadGame()
        {
            NetworkManager.singleton.StartHost();
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}
