using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace ToyTanks
{
    [RequireComponent(typeof(Rigidbody), typeof(Health))]
    public class TankController : NetworkBehaviour
    {
        [SyncVar] [SerializeField] private float _engineTorque = 150f;


        [SerializeField] private List<WheelCollider> _leftTrack;
        [SerializeField] private List<WheelCollider> _rightTrack;


        [SerializeField] private MonoBehaviour _inputHandler;

        [SerializeField] private TankWeapon _tankWeapon;


        private Rigidbody _rigidBody;


        private float _leftTrackInputValue = 0;
        private float _rightTrackInputValue = 0;

        private void Awake()
        {
            _rigidBody = gameObject.GetComponent<Rigidbody>();
        }

        private void Start()
        {
            if (_inputHandler != null)
            {
                _inputHandler.enabled = hasAuthority;
            }
        }

        private void FixedUpdate()
        {
            if (hasAuthority)
            {
                foreach (var wheelCollider in _leftTrack)
                {
                    wheelCollider.motorTorque = _leftTrackInputValue * _engineTorque;
                }

                foreach (var wheelCollider in _rightTrack)
                {
                    wheelCollider.motorTorque = _rightTrackInputValue * _engineTorque;
                }
            }
        }


        public void LeftTracks(InputAction.CallbackContext value)
        {
            if (!hasAuthority) return;
            var trackValue = value.ReadValue<float>();
            _leftTrackInputValue = trackValue;
        }

        public void RightTracks(InputAction.CallbackContext value)
        {
            if (!hasAuthority) return;
            var trackValue = value.ReadValue<float>();
            _rightTrackInputValue = trackValue;
        }


        public void ShootCannon()
        {
            if (hasAuthority)
            {
                CmdShootCannon();
            }
        }


        [Command]
        private void CmdShootCannon()
        {
            _tankWeapon.Shoot();
        }
    }
}