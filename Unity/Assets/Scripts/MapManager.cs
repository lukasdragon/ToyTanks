using System.Collections.Generic;
using System.IO;
using Mirror;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace ToyTanks
{
    public class MapManager : NetworkBehaviour
    {
        [SerializeField] private AssetReference testWallReference;
        [SerializeField] private AssetReference playerSpawnReference;
        [SerializeField] private AssetReference tilemapVisual;

        [SerializeField] private string defaultMapName;
        
        [SyncVar(hook = nameof(SetMapName))]
        private string _mapName = string.Empty;


        private GameObject _floor;
        private GameObject _walls;
        private GameObject _entities;

        private readonly List<GameObject> _spawnedObjects = new List<GameObject>();


        private Tilemap _tilemap;


        public override void OnStartServer()
        {
            SetMapName(_mapName, defaultMapName);
            base.OnStartServer();
        }

        private void OnEnable()
        {
            ClearMap();

            if (isClient && _mapName != null)
            {
                LoadMap(_mapName);
            }
        }

        private void OnDisable()
        {
            ClearMap();
        }

        private void SetMapName(string oldValue, string newValue)
        {
            if (string.IsNullOrEmpty(newValue))
            {
                ClearMap();
            }
            else
            {
                LoadMap(newValue);
            }
        }

        public void LoadMap(string mapName)
        {
            ClearMap();

            if (isServer)
            {
                _mapName = mapName;
            }

            var map = GetMap(mapName);
            GenerateMap(map);
        }

        private Map.Map GetMap(string mapName)
        {
            var mapJson = Addressables.LoadAssetAsync<TextAsset>($"Maps/{mapName.ToLower()}").WaitForCompletion();

            if (mapJson == null)
            {
                throw new FileLoadException(
                    $"Map Name invalid or addressable asset not configured correctly. Ensure map exists at Maps/{mapName.ToLower()}");
            }

            var gameMap = JsonConvert.DeserializeObject<Map.Map>(mapJson.text);

            return gameMap;
        }

        private void GenerateMap(Map.Map map)
        {
            GenerateFloor(map);
            GenerateWalls(map);
            GenerateEntities(map);
        }

        public void ClearMap()
        {
            foreach (var obj in _spawnedObjects)
            {
                Destroy(obj);
            }

            _spawnedObjects.Clear();
        }


        private void GenerateFloor(Map.Map map)
        {
            _floor = new GameObject("Floor");
            _spawnedObjects.Add(_floor);

            var tileMapVisualGameObject = Addressables
                .InstantiateAsync(tilemapVisual, new Vector3(-2, 0, -2), Quaternion.Euler(90, 0, 0), _floor.transform)
                .WaitForCompletion();

            var tileMapVisual = tileMapVisualGameObject.GetComponent<TilemapVisual>() as TilemapVisual;


            var floorHitBox = new GameObject("Floor Hitbox");
            floorHitBox.transform.parent = _floor.transform;
            floorHitBox.transform.position = new Vector3(-2, 0, -2);

            var boxCollider = floorHitBox.AddComponent<BoxCollider>();
            boxCollider.size = new Vector3(map.SizeX * 4, 1, map.SizeY * 4);
            boxCollider.center = new Vector3(map.SizeX * 4f / 2f, -0.5f, map.SizeY * 4f / 2f);


            _tilemap = new Tilemap(map.SizeX, map.SizeY, 4f, new Vector3(0, 0, 0));
            _tilemap.SetTilemapVisual(tileMapVisual);


            float xPos = 0;
            float zPos = 0;
            foreach (var tile in map.Ground)
            {
                if (tile != 0)
                {
                    _tilemap.SetTilemapSprite(new Vector3(xPos * 4, zPos * 4),
                        Tilemap.TilemapObject.TilemapSprite.Ground);
                }

                xPos += 1;

                if (xPos >= map.SizeX)
                {
                    xPos = 0;
                    zPos += 1;
                }
            }
        }

        private void GenerateWalls(Map.Map map)
        {
            _walls = new GameObject("Walls");
            _spawnedObjects.Add(_walls);

            float xPos = 0;
            float zPos = 0;
            foreach (var tile in map.Walls)
            {
                if (tile != 0)
                {
                    testWallReference.InstantiateAsync(new Vector3(xPos * 4, 2, zPos * 4), Quaternion.identity,
                        _walls.transform);
                }

                xPos += 1;

                if (xPos >= map.SizeX)
                {
                    xPos = 0;
                    zPos += 1;
                }
            }
        }

        private void GenerateEntities(Map.Map map)
        {
            _entities = new GameObject("Entities");
            _spawnedObjects.Add(_entities);

            foreach (var entity in map.Entities)
            {
                playerSpawnReference.InstantiateAsync(new Vector3(entity.PosX * 4, 1, entity.PosY * 4),
                    Quaternion.identity,
                    _entities.transform);
            }
        }
    }
}
