﻿using System;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using ToyTanks.Attributes;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ToyTanks.Map
{
    [Serializable]
    public class Map
    {
        [SerializeField] public string Version { get; set; }
        [SerializeField] public int SizeX { get; set; }
        [SerializeField] public int SizeY { get; set; }


        [SerializeField] public int[] Ground { get; set; }
        [SerializeField] public int[] Walls { get; set; }

        [SerializeField] public Entity[] Entities { get; set; }


        public static Map LoadFromString(string value)
        {
            return JsonConvert.DeserializeObject<Map>(value);
        }

        public class Entity
        {
            public int PosX { get; set; }
            public int PosY { get; set; }
            public string EntityType { get; set; }
        }
    }
}