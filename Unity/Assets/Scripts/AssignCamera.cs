using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Mirror;
using UnityEngine;

namespace ToyTanks
{
    public class AssignCamera : MonoBehaviour
    {
        private CinemachineVirtualCameraBase _cameraBase;

        private void Start()
        {
            var identity = GetComponent<NetworkIdentity>();

            if (!identity.hasAuthority) return;
            _cameraBase = GameObject.FindGameObjectWithTag("PlayerCamera").GetComponent<CinemachineVirtualCameraBase>();
            _cameraBase.Follow = this.transform;
            _cameraBase.Priority = 100;
        }

        private void OnDestroy()
        {
            if (_cameraBase == null) return;
            _cameraBase.Priority = 0;
            _cameraBase.Follow = null;
        }
    }
}