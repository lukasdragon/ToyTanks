namespace ToyTanks.Attributes
{
    [System.AttributeUsage(System.AttributeTargets.Method | System.AttributeTargets.Constructor)]
    public class PreserveAttribute : System.Attribute
    {
    }
}
