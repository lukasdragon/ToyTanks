using MoreMountains.Tools;

namespace ToyTanks.Events
{
    public struct GameStateEvent
    {
        public GameEventEnum GameEventName;
        public string EventValue;

        public GameStateEvent(GameEventEnum gameEvent, string value)
        {
            GameEventName = gameEvent;
            EventValue = value;
        }

        static GameStateEvent e;

        public static void Trigger(GameEventEnum gameEvent, string value)
        {
            e.GameEventName = gameEvent;
            e.EventValue = value;
            MMEventManager.TriggerEvent(e);
        }

        public enum GameEventEnum
        {
            LoadMap
        }
    }
}