using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

namespace ToyTanks
{
    public class TankWeapon : NetworkBehaviour
    {
        [SerializeField] private Transform _muzzlePosition;
        [SerializeField] private GameObject _bullet;

        [SyncVar] private float _currentShotCooldown = 0f;
        [SerializeField] [SyncVar] private float _shotCooldown = 1f;
        [SyncVar] private int numberOfShots;


        private void Update()
        {
            if (isServer)
            {
                if (_currentShotCooldown >= 0)
                {
                    _currentShotCooldown -= 1 * Time.deltaTime;
                }
            }
        }

        [Server]
        public void Shoot()
        {
            if (_currentShotCooldown <= 0)
            {
                _currentShotCooldown = _shotCooldown;
                RpcShootCannon();

                var bullet = Instantiate(_bullet, _muzzlePosition.position, _muzzlePosition.rotation);

                NetworkServer.Spawn(bullet);
            }
        }


        [ClientRpc]
        private void RpcShootCannon()
        {
            
        }



    }
}