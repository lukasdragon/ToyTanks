using Mirror;
using MoreMountains.Tools;
using Sirenix.OdinInspector;
using ToyTanks.Events;
using UnityEngine;

namespace ToyTanks.Networking
{
    public class ToyTanksNetworkManager : NetworkManager
    {
        public override void OnStartServer()
        {
            base.OnStartServer();
        }

        public override void OnStopServer()
        {
            base.OnStopServer();
        }

        public override void OnStopClient()
        {
            base.OnStopClient();
        }

        public override void OnClientDisconnect(NetworkConnection conn)
        {
            base.OnClientDisconnect(conn);
        }
    }
}
