﻿using Mirror;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.Serialization;

namespace ToyTanks
{
    [ShowOdinSerializedPropertiesInInspector]
    public class SerializedNetworkBehaviour : NetworkBehaviour, ISerializationCallbackReceiver,
        ISupportsPrefabSerialization
    {
        [SerializeField, HideInInspector] private SerializationData _serializationData;

        SerializationData ISupportsPrefabSerialization.SerializationData
        {
            get => this._serializationData;
            set => this._serializationData = value;
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            UnitySerializationUtility.DeserializeUnityObject(this, ref this._serializationData);
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            UnitySerializationUtility.SerializeUnityObject(this, ref this._serializationData);
        }
    }
}