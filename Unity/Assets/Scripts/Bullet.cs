using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.Serialization;

namespace ToyTanks
{
    [RequireComponent(typeof(Rigidbody))]
    public class Bullet : NetworkBehaviour
    {
        private Rigidbody _rigidbody;
        [SerializeField] private float _speed = 15;
        [SerializeField] private int _numberOfBounces = 5;

        [SerializeField] [SyncVar] private float _damage = 1f;

        private void Start()
        {
            _rigidbody = GetComponent(typeof(Rigidbody)) as Rigidbody;

            if (_rigidbody != null)
            {
                _rigidbody.AddRelativeForce(Vector3.forward * _speed, ForceMode.Impulse);
            }
            else
            {
                Debug.LogWarning("Bullet does not have a Rigidbody", this);
            }
        }


        private void OnCollisionEnter(Collision other)
        {
            if (isServer)
            {
                if (other.gameObject.CompareTag("Player"))
                {
                    var healthComponent = other.gameObject.GetComponent<Health>();
                    healthComponent.AddDamage(_damage);
                    Destroy(this.gameObject);
                }

                if (_numberOfBounces > 0)
                {
                    _numberOfBounces -= 1;
                }
                else
                {
                    NetworkServer.UnSpawn(this.gameObject);
                    Destroy(this.gameObject);
                }
            }
        }
    }
}