using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace ToyTanks
{
    public class Health : NetworkBehaviour
    {
        [SerializeField] [SyncVar] private float _maxHealth = 10f;
        [SyncVar] [HideInEditorMode] [Sirenix.OdinInspector.ShowInInspector] [ReadOnly] private float _health;
        [SyncVar] private Vector3 _spawnPoint;
        [SyncVar] private Quaternion _spawnRotation;

        [SerializeField] private bool _destroyOnDeath = false;

        [SerializeField] private UnityEvent _onDeath;


        private void Start()
        {
            if (isServer)
            {
                var transform1 = this.transform;
                _spawnPoint = transform1.position;
                _spawnRotation = transform1.rotation;
            }
        }

        private void Update()
        {
            if (isServer && _health <= 0)
            {
                Death();
            }
        }

        public float GetHealth()
        {
            return _health;
        }

        [Server]
        public void AddDamage(float damage)
        {
            _health -= damage;
        }


        [Server]
        private void Death()
        {
            if (_destroyOnDeath)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _health = _maxHealth;
                var transform1 = this.transform;
                transform1.position = _spawnPoint;
                transform1.rotation = _spawnRotation;
            }

            _onDeath.Invoke();
        }
    }
}