using ToyTanks.Editor.Importer;
using UnityEditor;
using UnityEditor.AssetImporters;
using UnityEngine;

namespace ToyTanks.Editor
{
    [CustomEditor(typeof(MapImporter))]
    public class MapImporterEditor : ScriptedImporterEditor
    {
        public override void OnInspectorGUI()
        {
            var floorStartIndex = new GUIContent("Floor Index Start");
            var floorStartIndexProperty = serializedObject.FindProperty("FloorStartingIndex");
            EditorGUILayout.PropertyField(floorStartIndexProperty, floorStartIndex);

            var wallStartIndex = new GUIContent("Wall Index Start");
            var wallStartIndexProperty = serializedObject.FindProperty("WallStartingIndex");
            EditorGUILayout.PropertyField(wallStartIndexProperty, wallStartIndex);
            
            
            
            base.ApplyRevertGUI();
        }
    }
}