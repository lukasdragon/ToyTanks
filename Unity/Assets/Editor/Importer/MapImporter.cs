using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using UnityEditor.AssetImporters;
using UnityEngine;

namespace ToyTanks.Editor.Importer
{
    [ScriptedImporter(1, "tmx")]
    public class MapImporter : ScriptedImporter
    {
        public int FloorStartingIndex = 1;
        public int WallStartingIndex = -1;


        public override void OnImportAsset(AssetImportContext ctx)
        {
            var path = ctx.assetPath;


            var xmlMap = DeserializeToObject<Map.XML.Map>(path);

            int.TryParse(xmlMap.Width, out var mapHeight);
            int.TryParse(xmlMap.Width, out var mapWidth);


            var groundLayer = StringToIntList(xmlMap.Layer[0].Data.Text).ToList();
            var wallLayer = StringToIntList(xmlMap.Layer[1].Data.Text).ToList();


            if (WallStartingIndex == -1)
            {
                WallStartingIndex = groundLayer.Max();
            }


            for (var i = 0; i < groundLayer.Count; i++)
            {
                if (groundLayer[i] != 0)
                {
                    groundLayer[i] = groundLayer[i] - FloorStartingIndex;
                }
            }

            for (var i = 0; i < wallLayer.Count; i++)
            {
                if (wallLayer[i] != 0)
                {
                    wallLayer[i] = wallLayer[i] - WallStartingIndex;
                }
            }


            List<Map.Map.Entity> entities = new List<Map.Map.Entity>(xmlMap.Objectgroup.Object.Count);
            foreach (var obj in xmlMap.Objectgroup.Object)
            {
                var ent = new Map.Map.Entity
                {
                    EntityType = obj.Type,
                    PosX = (int) (float.Parse(obj.X) / 32f),
                    PosY = (int) (float.Parse(obj.Y) / 32f)
                };
                entities.Add(ent);
            }


            var map = new Map.Map
            {
                Version = $"Alpha-{xmlMap.Version}",
                SizeY = mapHeight,
                SizeX = mapWidth,
                Ground = groundLayer.ToArray(),
                Walls = wallLayer.ToArray(),
                Entities = entities.ToArray()
            };


            var jsonString = JsonConvert.SerializeObject(map, Formatting.None);


            var textAsset = new TextAsset(jsonString);

            ctx.AddObjectToAsset("main asset", textAsset);
            ctx.SetMainObject(textAsset);
        }

        private static T DeserializeToObject<T>(string filepath) where T : class
        {
            var ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using var sr = new StreamReader(filepath);
            return (T) ser.Deserialize(sr);
        }

        private static IEnumerable<int> StringToIntList(string str)
        {
            if (string.IsNullOrEmpty(str))
                yield break;

            foreach (var s in str.Split(','))
            {
                if (int.TryParse(s, out var num))
                    yield return num;
            }
        }
    }
}